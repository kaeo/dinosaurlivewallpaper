﻿#define UNITY_ANDROID_APP

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraMover : MonoBehaviour
{
	const string TAG_CAM_POS = "CameraPosition";
	
	Transform myTransform;
	
	#if UNITY_EDITOR || UNITY_ANDROID_APP
	SwipeDetector detector;
	#else
	SwipeDetectorLWP detector;
	#endif
	
	Transform[] cameraTransforms;
	
	int currentIndex = 0;
	
	Vector3 targetPosition;
	Vector3 currentPosition;
	Vector3 velo;
	
	// Put 1 or -1.
	public float moveDirection = 1.0f;

	// Use this for initialization
	void Awake()
	{
		myTransform = transform;
		
		#if UNITY_EDITOR || UNITY_ANDROID_APP
		detector = GetComponent<SwipeDetector>();
		#else
		detector = GetComponent<SwipeDetectorLWP>();
		#endif
		
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(TAG_CAM_POS);
		
		List<GameObject> list = new List<GameObject>();
		list.AddRange(gameObjects);
		list.Sort(CompareName);
		
		cameraTransforms = new Transform[list.Count];
		//list.CopyTo(cameraTransforms);
		for(int i = 0; i < cameraTransforms.Length; i++)
		{
			cameraTransforms[i] = list[i].transform;
		}
		
		if(moveDirection > 0f)
		{
			currentIndex = 0;
		}
		else 
		{
			currentIndex = cameraTransforms.Length - 1;
		}
		
		currentPosition = targetPosition = cameraTransforms[currentIndex].position;
	}
	
	int CompareName(GameObject a, GameObject b)
	{
		return string.Compare(a.name, b.name);
	}
	
	
	void OnEnable()
	{
		detector.onStartSwiping += OnStartSwiping;
		detector.onSwiping += OnSwiping;
		detector.onStopSwiping += OnStopSwiping;
	}
	
	void OnDisable()
	{
		detector.onStartSwiping -= OnStartSwiping;
		detector.onSwiping -= OnSwiping;
		detector.onStopSwiping -= OnStopSwiping;
	}
	
	void OnStartSwiping()
	{
	
	}
	
	const float CAMERA_SWIPE_MULTIPLIER = 0.003f; //0.01f;
	
	void OnSwiping(Vector3 delta, Vector3 displacement)
	{
		int nextIndex = GetNewCameraIndexBySwipeDirection(displacement.x);
		
		//Debug.Log ("delta: " + delta);
		//Debug.Log("dpi = " + Screen.dpi);
		
		if(nextIndex > 0 && nextIndex < cameraTransforms.Length - 1)
		{
			currentPosition.x += (delta.x * moveDirection) * CAMERA_SWIPE_MULTIPLIER;
			targetPosition = currentPosition;
		}
		
		MyGUIText.instance.SetLine(0, "dt:" + delta.ToString("F1") + " dp:" + displacement.ToString("F1"));
		MyGUIText.instance.SetLine(1, "Screen: " + Screen.width + ", " + Screen.height + " | dpi: " + Screen.dpi);
		MyGUIText.instance.SetLine(2, "CAMERA_SWIPE_MULTIPLIER: " + CAMERA_SWIPE_MULTIPLIER.ToString("F6"));
		MyGUIText.instance.SetLine(3, "333");
		MyGUIText.instance.SetLine(4, "444");
	}
	
	void OnStopSwiping(Vector3 displacement)
	{
		currentIndex = GetNewCameraIndexBySwipeDirection(displacement.x);
		
		targetPosition = cameraTransforms[currentIndex].position;
	}
	
	int GetNewCameraIndexBySwipeDirection(float displacementX)
	{
		int index = currentIndex;
		
		if((displacementX * moveDirection) > 0f)
		{
			if(index < cameraTransforms.Length - 1) ++index;
		}
		else
		{
			if(index > 0) --index;
		}
		
		return index;
	}

	// Update is called once per frame
	void Update()
	{
		currentPosition = Vector3.SmoothDamp(currentPosition, targetPosition, ref velo, 0.3f);
		
		myTransform.position = currentPosition;
	}
	
	string rotateCamString = "";
	void SetRotation(string s)
	{
		rotateCamString = s;
		MyGUIText.instance.SetLine(3, "rotateCam: " + rotateCamString);
	}
	
	string enableRainString = "";
	void EnableRain(string s)
	{
		enableRainString = s;
		MyGUIText.instance.SetLine(4, "enableRain: " + enableRainString);
	}
}

