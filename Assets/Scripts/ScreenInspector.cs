using UnityEngine;
using System.Collections;

public class ScreenInspector : MonoBehaviour
{
	public GUISkin skin;
	
	public bool showFull = false;
	
	Color[] colors = new Color[] { Color.white, Color.gray, Color.yellow, Color.red, Color.green, Color.blue };
	int colorIndex = 0;
	
	void OnGUI()
	{
		GUI.skin = skin;
		
		Rect rect = new Rect(0, 0, Screen.width >> 1, 50);
		
		/*
		Rect buttonRect = rect;
		buttonRect.width = buttonRect.height;
		if(GUI.Button(buttonRect, "+"))
		{
			showFull = !showFull;
		}
		
		buttonRect.x += buttonRect.width;
		if(GUI.Button(buttonRect, "c"))
		{
			colorIndex = (colorIndex + 1) % colors.Length;
		}
		
		buttonRect.x = Screen.width - buttonRect.width;
		if(GUI.Button(buttonRect, "-"))
		{
			enabled = false;
		}
		
		if(!showFull) return;
		*/
		
		rect.y += rect.height;
		DrawLabelWithShadow(rect, "Screen size: " + Screen.width + ", " + Screen.height);
		/*
		rect.y += rect.height;
		DrawLabelWithShadow(rect, "dpi: " + Screen.dpi);
		
		rect.y += rect.height;
		DrawLabelWithShadow(rect, "Orientation: " + Screen.orientation);
		*/
	}
	
	void DrawLabelWithShadow(Rect rect, string s)
	{
		GUIUtils.DrawLabelWithShadow(rect, s, colors[colorIndex], Color.black);
	}
}
