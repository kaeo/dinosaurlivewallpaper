﻿using UnityEngine;
using System.Collections;

public class DinosaurManager : MonoBehaviour
{
	public Transform[] dinosaurs;
	
	Vector3[] initialPositions;
	
	//Vector3 offset = new Vector3(-11.5f, -1.8f, 6.0f);

	// Use this for initialization
	void Start ()
	{
		initialPositions = new Vector3[dinosaurs.Length];
		for(int i = 0; i < initialPositions.Length; i++)
		{
			initialPositions[i] = dinosaurs[i].localPosition;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		for(int i = 0; i < dinosaurs.Length; i++)
		{
			if((dinosaurs[i].localPosition - initialPositions[i]).magnitude > 30.0f)
			{
				dinosaurs[i].localPosition = initialPositions[i] + Vector3.forward * Random.Range(0f, 10f);
			}
		}
	}
}
