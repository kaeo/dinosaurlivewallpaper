using UnityEngine;
using System.Collections;

public class DinosaurSpawner : MonoBehaviour 
{
	public GameObject dinoTemplate;
	
	public float interval = 2.0f;
	
	float timeAcc = 0.0f;
	
	public Transform[] spawnPoints;
	
	// Update is called once per frame
	void Update () 
	{
		timeAcc += Time.deltaTime;
		if(timeAcc >= interval)
		{
			timeAcc -= interval;
			
			SpawnDino();
		}
	}
	
	void SpawnDino()
	{
		GameObject go = Instantiate(dinoTemplate) as GameObject;
		
		int randNum = Random.Range(0, spawnPoints.Length);
		Transform selectedTrans = spawnPoints[randNum];
		
		Transform trans = go.transform;
		trans.position = selectedTrans.position;
		trans.rotation = selectedTrans.rotation;
	}
}
