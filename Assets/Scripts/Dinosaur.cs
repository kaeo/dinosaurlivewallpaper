using UnityEngine;
using System.Collections;

public class Dinosaur : MonoBehaviour 
{
	public float liveSpan = 20.0f;
	
	float timeAcc;
	
	Transform cachedTransform;
	
	bool wasVisibleOnce = false;
	
	public bool destroyByTime = true;
	public bool destroyByVisibility = true;
	
	void Start()
	{
		cachedTransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		DestroyByTime();

		DestroyByVisibility();
	}
	
	void DestroyByTime()
	{
		if(!destroyByTime) return;
		
		timeAcc += Time.deltaTime;
		if(timeAcc >= liveSpan)
		{
			Debug.Log ("Destroyed by time.");
			Destroy(gameObject);
		}
	}
	
	void DestroyByVisibility()
	{
		if(!destroyByVisibility) return;
		
		if(wasVisibleOnce == false)
		{
			wasVisibleOnce = IsVisible();
			Debug.Log ("wasVisibleOnce = " + wasVisibleOnce + " @" + Time.frameCount);
		}
		
		// Destroy by out of frustum.
		if(wasVisibleOnce && !IsVisible())
		{
			Debug.Log ("Destroyed by visibility @" + Time.frameCount);
			Destroy(gameObject);
		}
	}
	
	bool IsVisible()
	{
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		
		foreach(Renderer r in renderers)
		{
			if(r.isVisible)
			{
				return true;
			}
		}
		
		return false;
	}
	
}
