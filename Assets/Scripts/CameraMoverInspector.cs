#define UNITY_ANDROID_APP

using UnityEngine;
using System.Collections;

public class CameraMoverInspector : MonoBehaviour
{
	public GUISkin skin;
	
	#if UNITY_EDITOR || UNITY_ANDROID_APP
	SwipeDetector detector;
	#else
	SwipeDetectorLWP detector;
	#endif
	
	Vector3 startSwipePosition;
	Vector3 delta;
	Vector3 displacement;

	// Use this for initialization
	void Awake()
	{
		#if UNITY_EDITOR || UNITY_ANDROID_APP
		detector = GetComponent<SwipeDetector>();
		#else
		detector = GetComponent<SwipeDetectorLWP>();
		#endif
	}
	
	void OnEnable ()
	{
		detector.onStartSwiping += OnStartSwiping;
		detector.onSwiping += OnSwiping;
		detector.onStopSwiping += OnStopSwiping;
	}
	
	void OnDisable ()
	{
		detector.onStartSwiping -= OnStartSwiping;
		detector.onSwiping -= OnSwiping;
		detector.onStopSwiping -= OnStopSwiping;
	}
	
	void OnStartSwiping ()
	{
		startSwipePosition = detector.StartSwipePosition;
	}
	
	void OnSwiping (Vector3 delta, Vector3 displacement)
	{
		this.delta = delta;
		this.displacement = displacement;
	}
	
	void OnStopSwiping (Vector3 displacement)
	{
		this.displacement = displacement;
	}
	
	const int RECT_HEIGHT = 50;
	
	void OnGUI()
	{
		GUI.skin = skin;
		
		Rect rect = new Rect(0, Screen.height - RECT_HEIGHT * 4, Screen.width >> 1, RECT_HEIGHT);
		
		Rect buttonRect = rect;
		buttonRect.width = buttonRect.height;
		buttonRect.x = Screen.width - buttonRect.width;
		if(GUI.Button(buttonRect, "-"))
		{
			enabled = false;
		}
		
		DrawLabelWithShadow(rect, "Start: " + startSwipePosition);
		
		rect.y += rect.height;
		DrawLabelWithShadow(rect, "Delta: " + delta);
		
		rect.y += rect.height;
		DrawLabelWithShadow(rect, "Displacement: " + displacement);
		
		rect.y += rect.height;
		DrawLabelWithShadow(rect, "--++ Script: " + detector.ScriptName);
		
		
	}
	
	void DrawLabelWithShadow(Rect rect, string s)
	{
		GUIUtils.DrawLabelWithShadow(rect, s, Color.cyan, Color.black);
	}
}
