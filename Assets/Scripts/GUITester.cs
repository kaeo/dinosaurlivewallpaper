using UnityEngine;
using System.Collections;

public class GUITester : MonoBehaviour
{
	public Font font;
	
	GUIStyle style;

	// Use this for initialization
	void Start()
	{
		style = new GUIStyle();
		style.font = font;
		style.normal.textColor = Color.black;
	}
	
	void OnGUI()
	{
		Rect rect = new Rect(0, 0, 400, 50);
		for(int i = 0; i < 5; i++)
		{
			GUI.Label(rect, "i = " + i, style);
			rect.y += rect.height;
		}
	}
}
