using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyGUIText : MonoBehaviour
{
	static public MyGUIText instance;
	
	List<string> lines = new List<string>();
	
	void Awake()
	{
		if(instance != null)
		{
			Debug.LogError("instance != null");
			return;
		}
		instance = this;
	}
	
	//public string Text
	//{
	//	get { return guiText.text; }
	//	set { guiText.text = value; }
	//}
	
	public string GetLine(int index)
	{
		return index >= 0 && index < lines.Count ? lines[index] : "";
	}
	
	public void SetLine(int index, string text)
	{
		if(index >= 0)
		{
			while(index >= lines.Count)
			{
				lines.Add("");
			}
			
			lines[index] = text;
			
			guiText.text = FullText;
		}
	}
	
	public string FullText
	{
		get
		{
			string s = "";
			for(int i = 0; i < lines.Count; i++)
			{
				s += lines[i] + "\n";
			}
			return s;
		}
	}
}
