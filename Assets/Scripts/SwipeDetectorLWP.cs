using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwipeDetectorLWP : MonoBehaviour
{
	public delegate void OnStartSwipingDelegate();
	public delegate void OnSwipingDelegate(Vector3 delta, Vector3 displacement);
	public delegate void OnStopSwipingDelegate(Vector3 displacement);
	
	public event OnStartSwipingDelegate onStartSwiping;
	public event OnSwipingDelegate onSwiping;
	public event OnStopSwipingDelegate onStopSwiping;
	
	// Motion event action.
	const int ACTION_DOWN = 0;
	const int ACTION_MOVE = 2;
	const int ACTION_UP = 1;
	
	// Use this for initialization
	void Start()
	{
	}
	
	// Update is called once per frame
	void Update()
	{
	}
	
	List<TouchAttr> touches = new List<TouchAttr>();
	
	void SendTouchXY(string xy)
	{
		string[] coord = xy.Split (',');
		Vector3 pos = new Vector3 (float.Parse (coord[0]), (Screen.height - float.Parse (coord[1])), 0.0f);
		int action;
		int.TryParse(coord[2], out action);
		
		if(guiText != null) guiText.text = pos.ToString("F2");
		
		TouchAttr prev = touches.Count == 0 ? null : touches[touches.Count-1];
		TouchAttr attr = new TouchAttr(pos, Time.timeSinceLevelLoad, action);
		touches.Add(attr);
		
		if(action == ACTION_DOWN)
		{
			if(onStartSwiping != null)
			{
				onStartSwiping();
			}
		}
		else if(action == ACTION_MOVE)
		{
			if(onSwiping != null)
			{
				onSwiping(attr.pos - prev.pos, attr.pos - touches[0].pos);
			}
		}
		else if(action == ACTION_UP)
		{
			if(onStopSwiping != null)
			{
				onStopSwiping(attr.pos - touches[0].pos);
				
				touches.Clear();
			}
		}
	}
	
	public Vector3 StartSwipePosition
	{
		get { return touches != null && touches.Count > 0 ? touches[0].pos : Vector3.zero; }
	}
	
	public string ScriptName
	{
		get { return "SwipeDetectorLWP"; }
	}

}

public class TouchAttr
{
	public Vector3 pos;
	public float timeStamp;
	public int action;
	
	public TouchAttr(Vector3 pos, float timeStamp, int action)
	{
		this.pos = pos;
		this.timeStamp = timeStamp;
		this.action = action;
	}
}