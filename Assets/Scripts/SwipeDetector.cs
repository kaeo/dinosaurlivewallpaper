using UnityEngine;
using System.Collections;

public class SwipeDetector : MonoBehaviour
{
	public int mouseButton = 0;
	
	public float threshold = 0.01f;
	
	Vector3 prevMousePos;
	
	Vector3 startSwipePos;
	
	enum State
	{
		Nothing,
		Down,
		Move,
	}
	State state = State.Nothing;
	
	public delegate void OnStartSwipingDelegate();
	public delegate void OnSwipingDelegate(Vector3 delta, Vector3 displacement);
	public delegate void OnStopSwipingDelegate(Vector3 displacement);
	
	public event OnStartSwipingDelegate onStartSwiping;
	public event OnSwipingDelegate onSwiping;
	public event OnStopSwipingDelegate onStopSwiping;

	// Use this for initialization
	void Start()
	{
	}
	
	void OnEnable()
	{
		prevMousePos = Input.mousePosition;
	}
	
	// Update is called once per frame
	void Update()
	{
		Vector3 diff;
		
		switch(state)
		{
		case State.Nothing:
			if(Input.GetMouseButtonDown(mouseButton))
			{
				if(onStartSwiping != null) onStartSwiping();
				state = State.Down;
				startSwipePos = Input.mousePosition;
			}
			break;
			
		case State.Down:
			diff = Input.mousePosition - startSwipePos;
			if(Mathf.Abs(diff.x) > threshold || Mathf.Abs(diff.y) > threshold)
			{
				if(onSwiping != null) onSwiping(diff, Input.mousePosition - startSwipePos);
				prevMousePos = Input.mousePosition;
				state = State.Move;
			}
			break;
			
		case State.Move:
			diff = Input.mousePosition - prevMousePos;
			if(onSwiping != null)
			{
				onSwiping(diff, Input.mousePosition - startSwipePos);
			}
			
			if(Input.GetMouseButtonUp(mouseButton))
			{
				if(onStopSwiping != null) onStopSwiping(Input.mousePosition - startSwipePos);
				state = State.Nothing;
			}
			prevMousePos = Input.mousePosition;
			break;
		}
	}
	
	public Vector3 StartSwipePosition
	{
		get { return startSwipePos; }
	}
	
	public string ScriptName
	{
		get { return "SwipeDetector"; }
	}
}
