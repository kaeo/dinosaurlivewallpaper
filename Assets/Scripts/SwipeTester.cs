﻿using UnityEngine;
using System.Collections;

public class SwipeTester : MonoBehaviour
{
	SwipeDetector detector;

	void Awake ()
	{
		Debug.Log("SwipeTester.Start");
		detector = GetComponent<SwipeDetector>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void OnEnable ()
	{
		Debug.Log("SwipeTester.OnEnable");
		detector.onStartSwiping += OnStartSwiping;
		detector.onSwiping += OnSwiping;
		detector.onStopSwiping += OnStopSwiping;
	}
	
	void OnDisable ()
	{
		Debug.Log("SwipeTester.OnDisable");
		detector.onStartSwiping -= OnStartSwiping;
		detector.onSwiping -= OnSwiping;
		detector.onStopSwiping -= OnStopSwiping;
	}
	
	void OnStartSwiping ()
	{
	
	}
	
	void OnSwiping (Vector3 delta, Vector3 displacement)
	{
		Debug.Log("displacement: " + displacement);
		if(displacement.sqrMagnitude > 100f)
		{
			
		}
	}
	
	void OnStopSwiping (Vector3 displacement)
	{
	
	}
}
