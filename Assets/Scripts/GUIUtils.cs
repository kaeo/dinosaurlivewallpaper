using UnityEngine;
using System.Collections;

public class GUIUtils
{
	static public void DrawLabelWithShadow(Rect rect, string s)
	{
		DrawLabelWithShadow(rect, s, Color.white, Color.black);
	}
	
	static public void DrawLabelWithShadow(Rect rect, string s, Color foreColor, Color backColor)
	{
		DrawLabelWithShadow(rect, s, foreColor, backColor, Vector2.one);
	}
	
	static public void DrawLabelWithShadow(Rect rect, string s, Color foreColor, Color backColor, Vector2 offset)
	{
		Rect shadowRect = rect;
		shadowRect.x += offset.x;
		shadowRect.y += offset.y;
		
		GUI.color = backColor;
		GUI.Label(shadowRect, s);
		
		GUI.color = foreColor;
		GUI.Label(rect, s);
	}
}
